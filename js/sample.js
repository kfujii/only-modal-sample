(function($) {
  var $content = $('#js-modal-template').html();

  $('.js-modal-open').click(function(e) {
    e.preventDefault();
    $.onlyModal.open($content);
  });

  // URL履歴にPUSH
  // open()の第二引数にURLを追加
  // 「戻る」でモーダルが消えます
  $('.js-modal-open-pushstate').click(function(e) {
    e.preventDefault();
    $.onlyModal.open($content, './push-state-sample.html');
  });

  $(document).on(
    'click',
    '.js-btn-modal-close',
    function(e) {
      e.preventDefault();
      $.onlyModal.close();
    }
  );

})(jQuery);

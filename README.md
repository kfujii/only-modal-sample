# サンプル

## 動作環境

* bowerでプラグインをインストール
* npm で http-serverをインストールしてサーバーを起動

```sh
cd [このファイルがあるディレクトリ]
bower install
npm install
npm start
```

